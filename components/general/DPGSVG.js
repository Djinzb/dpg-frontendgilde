import React from 'react';

const DPGSVG = () => {
  return (
    <>
      <svg viewBox="0 0 170 141" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
          d="M29.0884 74.9459H0.0901794V122.622H29.0884V74.9459Z"
          fill="#783C96"
        />
        <path
          d="M169.68 38.8231H140.682V89.961H169.68V38.8231Z"
          fill="#FABB22"
        />
        <path
          d="M75.9521 38.8231H46.9539V140.464H75.9521V38.8231Z"
          fill="#D23278"
        />
        <path
          d="M122.791 0.117203H93.8429V122.617H122.791V0.117203Z"
          fill="#E6463C"
        />
      </svg>
    </>
  );
};

export default DPGSVG;
