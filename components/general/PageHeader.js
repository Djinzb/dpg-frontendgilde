import React from 'react';
import styled from 'styled-components';

const StyledPageHeader = styled.div`
  width: 100vw;
  height: 4rem;
  margin: 1rem 0;
  display: flex;
  align-items: center;
  justify-content: space-around;
  svg {
    height: 70%;
    &:first-child {
      height: 30%;
    }
    &:last-child {
      height: 30%;
    }
    &:nth-child(2) {
      background: ${props => props.color};
    }
  }
`;

const PageHeader = ({ children, color, skew }) => {
  return (
    <StyledPageHeader skew={skew} color={color}>
      {children}
    </StyledPageHeader>
  );
};

export default PageHeader;
