import React from 'react';
import styled from 'styled-components';

const StyledThemeToggle = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid;
  top: 1rem;
  right: 1rem;
  z-index: 999;
  cursor: pointer;
  border-radius: 0.1rem;
  width: 1rem;
  height: 1rem;
  padding: 1rem;

  .modeText {
    font-size: 0.4rem;
    text-align: center;
  }
`;
const ToggleDarkMode = ({ theme, toggleTheme }) => {
  const isLight = theme === 'light';
  return (
    <StyledThemeToggle onClick={toggleTheme}>
      {isLight ? (
        <span>
          <i className="fas fa-sun"></i>
          <div className="modeText">Light</div>
        </span>
      ) : (
        <span>
          <i className="far fa-moon"></i>
          <div className="modeText">Dark</div>
        </span>
      )}
    </StyledThemeToggle>
  );
};

export default ToggleDarkMode;
