import React from 'react';
import styled from 'styled-components';
import PageHeader from '../general/PageHeader';
import { Nav1, Nav2, Nav3 } from '../hero/svgs';

const WhoIsWho = styled.section`
  width: 100vw;
  height: 100vh;
  position: relative;
`;

const index = ({ svgFill }) => {
  return (
    <WhoIsWho id="whoiswho">
      <PageHeader color="#D23278">
        <Nav1 svgFill={svgFill} />
        <Nav2 svgFill={svgFill} />
        <Nav3 svgFill={svgFill} />
      </PageHeader>
    </WhoIsWho>
  );
};

export default index;
