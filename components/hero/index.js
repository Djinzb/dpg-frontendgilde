import React from 'react';
import styled from 'styled-components';
import { Logo, Nav1, Nav2, Nav3 } from './svgs.js';

const Hero = styled.nav`
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  svg {
    width: 40vw;
  }
`;

const HeroNavItems = styled.div`
  width: 45vw;
  margin-top: 5rem;
  display: flex;
  justify-content: space-between;
`;

const HeroNavItem = styled.div`
  position: relative;
  height: 3rem;
  z-index: 2;
  cursor: pointer;
  svg {
    width: 8rem;
  }

  &:before {
    content: ' ';
    display: block;
    height: 100%;
    width: 120%;
    margin-left: -3px;
    margin-right: -3px;
    position: absolute;
    background-image: ${props => props.color};
    background-position: 0;
    background-size: 200%;
    transform: ${props => props.skew};
    top: -5%;
    left: -5%;
    border-radius: 20% 25% 20% 24%;
    padding: 10px 3px 3px 10px;
    transition: all 0.2s;
    z-index: -1;
  }
  &:hover {
    &:before {
      background-position: -99%;
    }
  }
`;

const index = ({ svgFill }) => {
  return (
    <Hero>
      <Logo svgFill={svgFill} />
      <HeroNavItems>
        <a href="#next">
          <HeroNavItem
            skew="rotate(-2deg)"
            color="linear-gradient(to right, transparent 50.5%, #783C96 50%)">
            <Nav1 svgFill={svgFill} />
          </HeroNavItem>
        </a>
        <a href="#whoiswho">
          <HeroNavItem
            skew="rotate(3deg)"
            color="linear-gradient(to right, transparent 50.5%, #D23278 50%)">
            <Nav2 svgFill={svgFill} />
          </HeroNavItem>
        </a>
        <a href="#uses">
          <HeroNavItem
            skew="rotate(-3deg)"
            color="linear-gradient(to right, transparent 50.5%, #FABB22 50%)">
            <Nav3 svgFill={svgFill} />
          </HeroNavItem>
        </a>
      </HeroNavItems>
    </Hero>
  );
};

export default index;
