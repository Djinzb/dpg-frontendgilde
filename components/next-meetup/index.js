import React from 'react';
import styled from 'styled-components';
import PageHeader from '../general/PageHeader';
import { Nav1, Nav2, Nav3 } from '../hero/svgs';

const NextMeetup = styled.section`
  width: 100vw;
  height: 100vh;
  position: relative;
`;

const index = ({ svgFill }) => {
  return (
    <NextMeetup id="next">
      <PageHeader color="#783C96">
        <Nav2 svgFill={svgFill} />
        <Nav1 svgFill={svgFill} />
        <Nav3 svgFill={svgFill} />
      </PageHeader>
    </NextMeetup>
  );
};

export default index;
