import React from 'react';
import styled from 'styled-components';
import PageHeader from '../general/PageHeader';
import { Nav1, Nav2, Nav3 } from '../hero/svgs';

const Uses = styled.section`
  width: 100vw;
  height: 100vh;
  position: relative;
`;

const index = ({ svgFill }) => {
  return (
    <Uses id="uses">
      <PageHeader color="#FABB22">
        <Nav2 svgFill={svgFill} />
        <Nav3 svgFill={svgFill} />
        <Nav1 svgFill={svgFill} />
      </PageHeader>
    </Uses>
  );
};

export default index;
