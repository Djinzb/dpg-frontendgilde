import React from 'react';
import { ThemeProvider } from 'styled-components';
import { useDarkMode } from '../components/custom-hooks/useDarkMode';
import { lightTheme, darkTheme } from '../themes/theme';
import { GlobalStyles } from '../themes/global';
import Head from '../components/general/head';
import ToggleDarkMode from '../components/general/ToggleDarkMode';

function App({ Component, pageProps }) {
  const [theme, toggleTheme] = useDarkMode();

  return (
    <ThemeProvider theme={theme === 'light' ? darkTheme : lightTheme}>
      <>
        <Head />
        <GlobalStyles />
        <Component {...pageProps} theme={theme} />
        <ToggleDarkMode theme={theme} toggleTheme={toggleTheme} />
      </>
    </ThemeProvider>
  );
}

export default App;
