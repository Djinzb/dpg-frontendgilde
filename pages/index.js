import React, { useState } from 'react';
import styled from 'styled-components';
import Nav from '../components/hero';
import NextMeetup from '../components/next-meetup';
import WhoIsWho from '../components/who-is-who';
import Uses from '../components/uses';

const StyledHero = styled.section`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Home = ({ theme }) => {
  const svgFill = theme === 'dark' ? '#363636' : 'white';
  return (
    <main>
      <StyledHero>
        <Nav svgFill={svgFill} />
      </StyledHero>
      <NextMeetup svgFill={svgFill} />
      <WhoIsWho svgFill={svgFill} />
      <Uses svgFill={svgFill} />
    </main>
  );
};

export default Home;
