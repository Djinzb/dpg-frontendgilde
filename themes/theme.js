export const lightTheme = {
  body: 'white',
  text: '#363636',
  border1: '1px solid #c3c3c3',
  border2: '2px solid #c3c3c3',
  border3: '3px solid #c3c3c3',
};

export const darkTheme = {
  body: '#363636',
  text: '#FAFAFA',
  border1: '1px solid #FAFAFA',
  border2: '2px solid #FAFAFA',
  border3: '3px solid #FAFAFA',
};
